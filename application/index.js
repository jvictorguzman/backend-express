var express = require('express');
const app = express();
var jwt = require('jsonwebtoken');

// los demás servicios que es necesario con token
app.get('/webapi/usuarios', validateToken, function(req, res) {
    const user = [
        { nombre: 'victor', universidad: 'uto', pais: 'bolivia'},
        {
            nombre: 'juan',
            universidad: 'san simon',
            pais: 'bolivia'
        }];

    res.json(user);
});


////////////////////////////////////////////////
// generar token en servicio de login 
app.post('/webapi/login', function(req, res) {
    const user = {userName: 'mateo'};
    const tokenGenerated = jwt.sign({user}, 'llave_poderosa_dificil_23@', {expiresIn: '60min'});
    res.json({
        nombreComplete: 'Victor Mateo',
        token: tokenGenerated
    });
});



app.get('/webapi', function(req, res) {
    res.json({
        message: 'bienvenido al servicio web de aqui'
    });
});



// definir un puerto
app.listen(3000, function(){
    console.log('servicio arriba');
});

// implementando el token
function validateToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if(bearerHeader) {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[0];
        req.token = bearerToken;
        jwt.verify(req.token, 'llave_poderosa_dificil_23@', function(error) {
            if(error) {
                res.sendStatus(403);
            } else {
                next();
            }
        }) ;  
    } else {
        res.sendStatus(403);
    }
}